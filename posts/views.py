from django.shortcuts import render, redirect, HttpResponseRedirect
from django.contrib.auth import authenticate, logout
from django.contrib.auth import login as auth_login
from django.contrib.auth.decorators import login_required
from django.db.utils import IntegrityError
# redireccionar por nombre de url
from django.urls import reverse
#modelos
from django.contrib.auth.models import User
from posts.models import Post

# Create your views here.

@login_required(login_url='login')
def perfil(request):
    posts = Post.objects.all().filter(user_id=request.user.id)
    return render(request,'perfil.html',{'posts':posts})


@login_required(login_url='login')
def subir_post(request):

    if request.method =='POST':
        titulo=request.POST.get('titulo')
        foto=request.FILES.get('foto')
        descripcion=request.POST.get('descripcion')
        user=request.user.id
        new_post=Post(titulo=titulo,foto_post=foto,descripcion=descripcion,user_id=user)
        new_post.save()
        return HttpResponseRedirect(reverse('perfil'))

    return render(request,'subir_post.html')

@login_required(login_url='login')
def post(request,id):
    posts = Post.objects.all().filter(id=id)
    
    return render(request,'post.html',{'posts':posts})

